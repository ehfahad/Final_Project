<?php

namespace App\Library;

use App\Message\Message;
use App\Utility\Utility;

class Library{
    public $id = "";


    public function __construct()
    {
        $this->conn = mysqli_connect("localhost", "root", "", "lms");
    }

    public function prepare($data)
    {
        if (array_key_exists('title', $data))
            $this->title = $data['title'];
        if (array_key_exists('id', $data))
            $this->id = $data['id'];
        if (array_key_exists("description", $data)) {
            $this->description = $data['description'];
        }
        if (array_key_exists("filterByTitle", $data)) {
            $this->filterByTitle = $data['filterByTitle'];
        }
        if (array_key_exists("filterByDescription", $data)) {
            $this->filterByDescription = $data['filterByDescription'];
        }
        if (array_key_exists("search", $data)) {
            $this->search = $data['search'];
        }
    }


    public function index()
    {
        $whereClause= " 1=1 ";
        if(!empty($this->filterByTitle)){
            $whereClause.=" AND  title LIKE '%".$this->filterByTitle."%'";
        }
        if(!empty($this->filterByDescription)){
            $whereClause.=" AND  description LIKE '%".$this->filterByDescription."%'";
        }
        if(!empty($this->search)){
            $whereClause.=" AND  description LIKE '%".$this->search."%' OR title LIKE '%".$this->search."%'";
        }


        $_allbook = array();
        $query = "SELECT * FROM `booklist` AND ".$whereClause;
        $result = mysqli_query($this->conn, $query);

        while ($row = mysqli_fetch_object($result)) {
            $_allbook[] = $row;
        }
        return $_allbook;
    }


    public function paginator($startFrom=0,$limit=5){
        $hobbies = array();
        $query = "SELECT * FROM `booklist` LIMIT ".$startFrom.",".$limit;
        $result = mysqli_query($this->conn,$query);
        while($row= mysqli_fetch_object($result))
            $hobbies[]=$row;
        return $hobbies;
    }


    public function count(){
        $query = "SELECT COUNT(*) AS total FROM `lms`.`booklist`";
        $result = mysqli_query($this->conn,$query);
        if($row= mysqli_fetch_assoc($result))
            return $row['total'];
    }
}